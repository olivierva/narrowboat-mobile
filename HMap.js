import React, {
    Component,
} from 'react';

import Svg,{
    Path,
} from 'react-native-svg';
import SvgPanZoom, { SvgPanZoomElement } from 'react-native-svg-pan-zoom';
const Honeycomb = require('honeycomb-grid');

class HMap extends Component<{}> {
    constructor(props){
        super(props);
        this.Hex = Honeycomb.extendHex({ size: 20 });
        this.Grid = Honeycomb.defineGrid(this.Hex);
    }

    render() {
        const paths = []
        this.Grid.rectangle({ width: 64, height: 64 }).map((hex, index) => {
            const point = hex.toPoint();
            // add the hex's position to each of its corner points
            const corners = hex.corners().map(corner => corner.add(point));
            const [c0, c1, c2, c3, c4, c5] = corners;
            paths.push(<HPath callback={this.props.callback} key={index} cube={hex.cube()} d={"M"+c0.x+" "+c0.y+" L"+c1.x+" "+c1.y+" L"+c2.x+" "+c2.y+" L"+c3.x+" "+c3.y+" L"+c4.x+" "+c4.y+" L"+c5.x+" "+c5.y+" Z"} />)
        });

        return (
            <SvgPanZoom
                canvasHeight  = {1000}
                canvasWidth   = {1000}
                minScale      = {0.1}
                initialZoom   = {1.1}
                onZoom        = {(zoom) => { console.log('onZoom:' + zoom) }}
                canvasStyle   = {{ backgroundColor: 'white' }}
                viewStyle     = {{ backgroundColor: 'green'  }}
            >
            {paths}
            </SvgPanZoom>
        )
    }
}

class HPath extends Component<{}> {
    constructor(props){
        super(props);
        this.state = { _fill: "none" };
        this.handlerButtonOnClick = this.handlerButtonOnClick.bind(this);
    }

    handlerButtonOnClick(){
        this.setState({ _fill: "blue" });
        this.props.callback(this.props.cube);
    }

    render() {
        return (
            <Path d={this.props.d}
                  fill={this.state._fill}
                  stroke="black"
                  onPress={this.handlerButtonOnClick}
            />
        )
    }
}

export default HMap;