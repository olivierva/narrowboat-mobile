/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View
} from 'react-native';
import PinchZoomView from 'react-native-pinch-zoom-view';
import HMap from './HMap.js';


const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
    android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component<{}> {

    constructor() {
        super();

        this.state = {
            open: false,
            connected: false
        };

        console.log("Creating websocket message");
        this.socket = new WebSocket('ws://localhost:9090/entry');
        this.socket.onopen = () => {
            this.setState({connected: true});
        };
        this.socket.onmessage = (e) => {
            // a message was received
            console.log(e.data);
        };

        this.socket.onerror = (e) => {
            // an error occurred
            console.log(e.message);
        };

        this.socket.onclose = (e) => {
            // connection closed
            console.log(e.code, e.reason);
        };
        this.emit = this.emit.bind(this);

    }

    emit() {
        if (this.state.connected) {
            this.socket.send("It worked!");
            this.setState(prevState => ({open: !prevState.open}))
        }
    }

    wsSendCallback = (data) => {
        console.log(data);
        this.socket.send(JSON.stringify(data))
    };

    render() {
        return (
            <PinchZoomView>
                <HMap callback={this.wsSendCallback}/>
            </PinchZoomView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
